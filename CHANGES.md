# Changelog

## 1.12 (2019-05-14)
-   Update results from search query all at once rather than looping.


## 1.11 (2019-04-22)
-   Update calls for calculating coincidence FARs to use strings rather than
    RAVEN class objects.


## 1.10 (2019-02-15)
-   Fix link in log message.


## 1.9 (2019-02-15)
-   Write and upload coincidence_far.json when computing temporal and
    spatiotemporal coincidence FARs. This will simplify matters when
    constructing the EM_COINC circulars.


## 1.8 (2018-10-03)
-   Fixed tagnames to tag_name when writing log comments in GraceDb.


## 1.7 (2018-09-26)
-   Use ligo.skymap.io module instead of deprecated lalinference.io module.

-   Added spatio-temporal coincidence FAR calculating ability that utilizes
    skymaps from both the LVC and Fermi.


## 1.6 (2018-09-24)
-   Update ligo.raven.search query and search methods to allow pipeline
    specification. Then, while searching for external triggers, we can
    distinguish between SNEWS and Fermi/Swift triggers.


## 1.5 (2018-08-14)
-   Update ligo.raven.search.calc_signif_gracedb to compute the FAR for
    coincidences between superevents and external triggers as opposed to GW
    triggers and external triggers.


## 1.4 (2018-08-14)
-   Option to pass group specification to ligo.raven.search and
    ligo.raven.query that filters out superevent search results depending on
    the group of the preferred_event


## 1.3 (2018-08-02)
-   Added dependency on ligo-segments

-   Work around missing six dependency in healpy 1.12.0

-   Debugged broken links in comments uploaded to GraceDb. For superevents,
    the links need to be /superevents not /events.

-   Debugged ligo.raven.gracedb_events.SE so that it has a graceid attribute

-   Update VOEventLib package version so that the bug found by Tanner P. is fixed

-   Handle searches with superevents

-   Option to pass an instance of GraceDb to ligo.raven.search and
    ligo.raven.gracedb_events; needed for implementation with GWCelery
    where we might be be using the default GraceDb url

-   Update call to GraceDb superevent object so that it uses superevent method
    vs superevents


## 1.1.dev0 (2018-06-19)

-   Renamed package to ligo-raven to avoid confusion and conflict with
    another package called raven on PyPI

-   Ported to Python 3 / Dropped Python 2 support entirely

-   Project handed off to Min-A Cho and Shaon Ghosh

 
## 1.0 (2016-11-03) Commit 0b3d37a7

-   Last commit by Alex Urban
